module org.utcn.dsrl.View {
    requires javafx.controls;
    requires javafx.fxml;
//    requires lombok;

//    opens org.utcn.dsrl.Controller to javafx.fxml;
    opens org.utcn.dsrl.View to javafx.fxml;
    exports org.utcn.dsrl.View;
    exports org.utcn.dsrl.Controller;
}