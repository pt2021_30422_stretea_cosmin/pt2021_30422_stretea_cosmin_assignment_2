package org.utcn.dsrl.Controller;


import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import org.utcn.dsrl.Model.Server;
import org.utcn.dsrl.Model.Task;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class SimulationFrameController {

    public static List<Server> finalServers;
    private static List<Task> finalGeneratedTasks;

    public static BooleanProperty changed = new SimpleBooleanProperty();
    public static BooleanProperty done = new SimpleBooleanProperty();
    public static BooleanProperty pauseExecution = new SimpleBooleanProperty();

    public static int finalNumberOfClients;
    public static int finalNumberOfServers;

    public static AtomicInteger averageWaitingTimeSum = new AtomicInteger(0);
    public static AtomicInteger averageServiceTimeSum = new AtomicInteger(0);
    public static AtomicInteger peakHour = new AtomicInteger(0);


    public synchronized static List<Server> getFinalServers() {
        return finalServers;
    }

    public static List<Task> getFinalGeneratedTasks() {
        return finalGeneratedTasks;
    }

    public static int getWaitingListSize(){return finalGeneratedTasks.size();}

    public static synchronized void update(List<Server> servers, List<Task> generatedTasks){
        finalServers = servers;
        finalGeneratedTasks = generatedTasks;
        changed.setValue(true);
        done.setValue(false);
    }

    public static synchronized void init(List<Server> servers, List<Task> generatedTasks, int numberOfClients, int numberOfServers){
        finalServers = Collections.synchronizedList(servers);
        finalGeneratedTasks = Collections.synchronizedList(generatedTasks);
        finalNumberOfClients = numberOfClients;
        finalNumberOfServers = numberOfServers;
        changed.setValue(false);
        done.setValue(false);
        pauseExecution.setValue(false);
        averageServiceTimeSum.set(0);
        averageWaitingTimeSum.set(0);
        peakHour.set(0);
    }


    public static String print(){
        int index = 0;
        String output = "";
        output+= "---------------------------\n";
        output+= "Servers: \n\n";
        index = 1;
        for(Server s : finalServers){ output+= "\t" + "[server " + index++ + "]\n" + s.toString() + "\n\n";}
        output+= "Clients queue: \n";
        for(Task t : finalGeneratedTasks){ output+= t.toString();}
        if(finalGeneratedTasks.size() == 0) output+= "\tEMPTY QUEUE\n";
        output+= "---------------------------\n\n";

        return output;
    }

    public static String simulationResult(){
        String output = "";
        output+= "---------------------------\nRESULTS:\n";
        output+= "\t Average Waiting Time: " + SimulationFrameController.getAverageWaitingTime() + "\n";
        output+= "\t Average Service Time: " + SimulationFrameController.getAverageServiceTime() + "\n";
        output+= "\t Peak Hour: " + SimulationFrameController.getPeakHour() + "\n";
        output+= "---------------------------\n";
        return output;
    }

    public static int getAverageWaitingTime(){
        return averageWaitingTimeSum.get()/finalNumberOfClients;
    }

    public static int getAverageServiceTime(){
        return averageServiceTimeSum.get()/finalNumberOfServers;
    }

    public static int getPeakHour(){
        return peakHour.get();
    }


    public static synchronized void reset() {
        for(Server s: finalServers){
            s.clear();
        }
        finalGeneratedTasks.clear();
        changed.setValue(false);
        done.setValue(true);
    }

}
