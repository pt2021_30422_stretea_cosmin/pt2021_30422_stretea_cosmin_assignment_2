package org.utcn.dsrl.Controller;

import javafx.fxml.Initializable;
import org.utcn.dsrl.Model.*;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class SimulationController implements Runnable {

    public int timeLimit = 10;
    public int maxProcessingTime = 5;
    public int minProcessingTime = 1;
    public int maxArrivalTime = 6;
    public int minArrivalTime = 1;
    public int numberOfServers = 2;
    public int numberOfClients = 10;
    public SelectionPolicy selectionPolicy = SelectionPolicy.SHORTEST_TIME;

    private Scheduler scheduler;
    private List<Task> generatedTasks;


//    public SimulationController(int timeLimit, int timeLimit, int timeLimit, ){
//        this.scheduler = new Scheduler(numberOfServers, numberOfClients);
//        this.scheduler.changeStrategy(selectionPolicy);
//        generatedTasks = generateNRandomTasks();
//        SimulationFrameController.init(this.scheduler.getServers(), generatedTasks);
//    }


    public SimulationController(int timeLimit, int numberOfServers, int numberOfClients,
                                int maxProcessingTime, int minProcessingTime, int maxArrivalTime, int minArrivalTime) {
        this.timeLimit = timeLimit;
        this.maxProcessingTime = maxProcessingTime;
        this.minProcessingTime = minProcessingTime;
        this.maxArrivalTime = maxArrivalTime;
        this.minArrivalTime = minArrivalTime;
        this.numberOfServers = numberOfServers;
        this.numberOfClients = numberOfClients;
        this.initVariables();
    }

    private void initVariables(){
        this.scheduler = new Scheduler(numberOfServers, numberOfClients);
        this.scheduler.changeStrategy(selectionPolicy);
        generatedTasks = generateNRandomTasks();
        SimulationFrameController.init(this.scheduler.getServers(), generatedTasks, numberOfClients, numberOfServers);
    }

    private synchronized List<Task> generateNRandomTasks(){
        List<Task> result = Collections.synchronizedList(new ArrayList<Task>());

        for(int i = 0; i < this.numberOfClients; i++){
            int processingTime = (int)(Math.random()* (maxProcessingTime - minProcessingTime) + minProcessingTime);
            int arrivalTime = (int)(Math.random()* (maxArrivalTime - minArrivalTime) + minArrivalTime);

            result.add(new Task(i, arrivalTime, processingTime));
        }

        Collections.sort(result, Comparator.comparingInt(Task::getArrivalTime));

        return result;
    }

    public void interrupt(){
        for(Server s : this.scheduler.getServers()){
            s.setRunning(false);
        }
    }

    @Override
    public void run(){
        int time = 0;
        Integer max = 0;
        String log = "";
        while((!generatedTasks.isEmpty() || this.scheduler.areServersRunning()) && time < timeLimit){
            if(SimulationFrameController.pauseExecution.get()){
                    continue;
            }

            int finalCurrentTime = time;
            List<Task> currTasks = generatedTasks
                    .stream()
                    .filter(o -> o.getArrivalTime() == finalCurrentTime)
                    .collect(Collectors.toList());

            generatedTasks = generatedTasks
                    .stream()
                    .filter(o -> o.getArrivalTime() > finalCurrentTime)
                    .collect(Collectors.toList());

            for(Task t : currTasks){
                this.scheduler.dispatchTask(t);
            }
            SimulationFrameController.update(this.scheduler.getServers(), generatedTasks);
            max = setPeakTime(max, finalCurrentTime);

            log+= "\n[time: " + time++ + " ]\n" + SimulationFrameController.print();
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        this.interrupt();
        SimulationFrameController.reset();
        generateLog(log, time);

    }

    private void generateLog(String log, int time){

        log+="\n[time: " + time + " ]\n" + SimulationFrameController.print() +
        "\n--------------------------\nALL TASKS WERE COMPLETED!\n--------------------------\n";
        log+="\n" + SimulationFrameController.simulationResult();

        try {
            FileWriter myWriter = new FileWriter("log.txt");
            myWriter.write(log);
            myWriter.close();
//            System.out.println("Successfully wrote to the file.");
        } catch (IOException e) {
//            System.out.println("An error occurred.");
            e.printStackTrace();
        }
    }

    private synchronized Integer setPeakTime(Integer currMax, int currTime){

        int count = SimulationFrameController.getFinalServers()
                .stream()
                .mapToInt(Server::getTasksSize)
                .sum();
//        for(Server s : SimulationFrameController.getFinalServers()){
//            count+= s.getTasksSize();
//        }

        if(count > currMax){
            currMax = count;
            SimulationFrameController.peakHour.set(currTime);
        }
        return currMax;
    }

    public void start(){
        Thread t = new Thread(this);
        t.start();
    }

//    private static Parent loadFXML(String fxml) throws IOException {
//        FXMLLoader fxmlLoader = new FXMLLoader(UserInterfaceController.class.getResource(fxml + ".fxml"));
//        return fxmlLoader.load();
//    }


//    public static void main(String[] args){
//        SimulationController.start();
//    }

}
