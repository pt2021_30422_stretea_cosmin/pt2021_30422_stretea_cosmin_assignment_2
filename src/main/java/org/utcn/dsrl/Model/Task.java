package org.utcn.dsrl.Model;

//import lombok.Getter;
//import lombok.Setter;

//@Getter
//@Setter
public class Task {

    private int arrivalTime;
    private int processingTime;
    private int timeInQueue;
    private int id;

    public Task(){
        this(0,0,0);
    }

    public Task(int id, int arrivalTime, int processingTime){
        this.id = id;
        this.arrivalTime = arrivalTime;
        this.processingTime = processingTime;
        this.timeInQueue = 0;
    }

    @Override
    public String toString() {
        return "\ttask { " +
                "id=" + id +
                ", arrivalTime=" + arrivalTime +
                ", processingTime=" + processingTime +
                " }\n";
    }

    public int getId() {
        return id;
    }

    public  synchronized int getTimeInQueue() {
        return timeInQueue;
    }

    public int getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(int arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    public int getProcessingTime() {
        return processingTime;
    }

    public void setProcessingTime(int processingTime) {
        this.processingTime = processingTime;
    }

    public void incrementTimeInQueue() {
        this.timeInQueue++;
    }

    public void decrementTimeInQueue() {
        this.timeInQueue--;
    }
}
