package org.utcn.dsrl.Model;


//import lombok.Getter;
//import lombok.Setter;

import org.utcn.dsrl.Controller.SimulationFrameController;

import java.util.ArrayList;
import java.util.Objects;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

//@Getter
//@Setter
public class Server implements Runnable{

    private LinkedBlockingQueue<Task> tasks;
    private AtomicInteger waitingPeriod;
    private AtomicBoolean running = new AtomicBoolean(false);


    public Server(){
        this.tasks = new LinkedBlockingQueue<Task>();
        this.waitingPeriod = new AtomicInteger(0);
    }

    public void addTask(Task newTask){
        try{
            this.tasks.add(newTask);
            this.waitingPeriod.getAndAdd(newTask.getProcessingTime());
        }catch (NullPointerException e){
            System.out.println("muie la add task sa mi fut una");
            e.printStackTrace();
        }

    }

    public void removeTask(Task toDelete){
        this.tasks.remove(toDelete);
    }

    public synchronized void run(){
        //where break?
        this.running.set(true);
        while(this.running.get()){
            if(this.tasks.isEmpty()){
                try {
                    Thread.sleep(1000);
                    continue;
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            try {
                //increment time in queue
                this.tasks.forEach(Task::incrementTimeInQueue);
                SimulationFrameController.averageServiceTimeSum.getAndAdd(1);
                Task currTask = null;
                currTask = this.tasks.peek();
                currTask.setProcessingTime(currTask.getProcessingTime() - 1);
                this.waitingPeriod.getAndAdd(-1);

                if(currTask.getProcessingTime() == 0){
                    SimulationFrameController.averageWaitingTimeSum.getAndAdd(Objects.requireNonNull(this.tasks.poll()).getTimeInQueue());
//                    System.out.println("MUIE LA AVG WAIT TIME: "+SimulationFrameController.averageWaitingTimeSum + "\n");
                }
//                int newWaitingPeriod = this.waitingPeriod.get() - currTask.getProcessingTime();
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public String toString() {
        String output = "";
        for(Task t : this.tasks){ output+= t.toString();}
        if(this.tasks.size() == 0) output+= "\tEMPTY\n";
        output+= "\tWAIT_PERIOD= " + waitingPeriod + '\n';

        return output;
    }

    public void setRunning(boolean state) {
        this.running.set(state);
    }

    public LinkedBlockingQueue<Task> getTasks(){
        return this.tasks;
    }

    public ArrayList<Task> getTasksAsArrayList(){
        return new ArrayList<Task>(this.tasks);
    }

    public void setTasks(LinkedBlockingQueue<Task> tasks) {
        this.tasks = tasks;
    }

    public void clear(){
        this.tasks.clear();
        this.waitingPeriod.set(0);
    }

    //
    public Integer getWaitingPeriodInteger() {
        return waitingPeriod.get();
    }

    public Integer getTasksSize(){
        return this.tasks.size();
    }
}
