package org.utcn.dsrl.Model;

import java.util.ArrayList;
import java.util.List;

public interface Strategy {
    public void addTask(ArrayList<Server> servers, Task t);
}
