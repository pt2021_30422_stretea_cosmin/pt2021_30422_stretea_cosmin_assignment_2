package org.utcn.dsrl.Model;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.NoSuchElementException;

public class ConcreteStrategyQueue implements Strategy{

    @Override
    public void addTask(ArrayList<Server> servers, Task t){
        Server currServer = servers
                .stream()
                .min(Comparator.comparing(Server::getTasksSize))
                .orElseThrow(NoSuchElementException::new);

        try{
            currServer.addTask(t);
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
