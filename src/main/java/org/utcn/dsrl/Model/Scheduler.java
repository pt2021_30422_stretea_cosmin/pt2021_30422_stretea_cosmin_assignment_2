package org.utcn.dsrl.Model;

import java.util.ArrayList;

public class Scheduler {

    private ArrayList<Server> servers;
    private int maxNoServers;
    private int maxTasksPerServer;
    private Strategy strategy;


    public Scheduler(int maxNoServers, int maxTasksPerServer){
        this.maxTasksPerServer = maxTasksPerServer;
        this.servers = new ArrayList<Server>();
        for(int i = 0; i < maxNoServers; i++){
            Server newServer = new Server();
            this.servers.add(newServer);
            Thread t = new Thread(newServer);
            t.start();
//            newServer.start();
        }

    }

    public void changeStrategy(SelectionPolicy policy){
        if(policy == SelectionPolicy.SHORTEST_QUEUE){
            strategy = new ConcreteStrategyQueue();
        }
        if(policy == SelectionPolicy.SHORTEST_TIME){
            strategy = new ConcreteStrategyTime();
        }
    }


    public boolean areServersRunning(){
        for(Server s : servers){
            if(s.getTasksSize() > 0){
                return true;
            }
        }
        return false;
    }


    public void dispatchTask(Task t){
        strategy.addTask(this.servers, t);
    }

    public ArrayList<Server> getServers(){
        return servers;
    }
}
