package org.utcn.dsrl.Model;

public enum SelectionPolicy {
    SHORTEST_QUEUE,
    SHORTEST_TIME
}
