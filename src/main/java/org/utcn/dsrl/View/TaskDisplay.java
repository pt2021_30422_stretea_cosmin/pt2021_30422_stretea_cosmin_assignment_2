package org.utcn.dsrl.View;

import javafx.scene.Node;
import javafx.scene.control.Button;
import org.utcn.dsrl.Model.Task;

public class TaskDisplay extends Button {

    private Task task;

    public TaskDisplay(Task o) {
        super();
        this.task = o;
        this.setText(String.valueOf(o.getId()));
        this.getStylesheets().add(UserInterfaceController.class.getResource("style.css").toExternalForm());
        this.getStyleClass().add("task-display");
    }
}
