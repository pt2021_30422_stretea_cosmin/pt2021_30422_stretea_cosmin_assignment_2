package org.utcn.dsrl.View;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import org.utcn.dsrl.Controller.SimulationController;
import org.utcn.dsrl.Controller.SimulationFrameController;
import org.utcn.dsrl.Model.Task;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


/**
 * JavaFX App
 */
public class UserInterfaceController {

    private static Integer currentTime;
    private static int waitingLine;

    @FXML
    public TextField peakHourOutput;

    @FXML
    public TextField avgServiceTimeOutput;

    @FXML
    public TextField avgWaitTimeOutput;

    @FXML
    public Button startButton;


    @FXML
    public TextField noClientsInput;

    @FXML
    public TextField noQueuesInput;

    @FXML
    public TextField maxTimeInput;

    @FXML
    public TextField minArrivalTimeInput;

    @FXML
    public TextField maxArrivalTimeInput;

    @FXML
    public TextField minServiceTimeInput;

    @FXML
    public TextField maxServiceTimeInput;

    @FXML
    private HBox QueuesHBox;

    @FXML
    public TextField waitingOutput;

    @FXML
    public TextField timeOutput;





    private void disableAllInputs() {
        startButton.setDisable(true);
        noClientsInput.setDisable(true);
        noQueuesInput.setDisable(true);
        maxTimeInput.setDisable(true);
        minServiceTimeInput.setDisable(true);
        maxServiceTimeInput.setDisable(true);
        minArrivalTimeInput.setDisable(true);
        maxArrivalTimeInput.setDisable(true);
        peakHourOutput.setDisable(true);
        avgWaitTimeOutput.setDisable(true);
        avgServiceTimeOutput.setDisable(true);
    }

    private void activateAllInputs() {
        startButton.setDisable(false);
        noClientsInput.setDisable(false);
        noQueuesInput.setDisable(false);
        maxTimeInput.setDisable(false);
        minServiceTimeInput.setDisable(false);
        maxServiceTimeInput.setDisable(false);
        minArrivalTimeInput.setDisable(false);
        maxArrivalTimeInput.setDisable(false);
        peakHourOutput.setDisable(false);
        avgWaitTimeOutput.setDisable(false);
        avgServiceTimeOutput.setDisable(false);
    }

    private boolean validInput(){
        if(
                maxTimeInput.getText().equals("")||
                noClientsInput.getText().equals("")||
                maxArrivalTimeInput.getText().equals("")||
                minArrivalTimeInput.getText().equals("")||
                maxServiceTimeInput.getText().equals("")||
                minServiceTimeInput.getText().equals("")
        ) return false;


        try{
            Integer maxTime = Integer.parseInt(maxTimeInput.getText());
            Integer noQueues = Integer.parseInt(noQueuesInput.getText());
            Integer noClients = Integer.parseInt(noClientsInput.getText());
            Integer maxServiceTime = Integer.parseInt(maxServiceTimeInput.getText());
            Integer minServiceTime = Integer.parseInt(minServiceTimeInput.getText());
            Integer maxArrivalTime = Integer.parseInt(maxArrivalTimeInput.getText());
            Integer minArrivalTime = Integer.parseInt(minArrivalTimeInput.getText());

            if(maxTime < 1 || maxTime > 99999) return false;
            if(noQueues < 1 || noQueues > 99999) return false;
            if(noClients < 1 || noClients > 99999) return false;
            if(maxServiceTime <= minServiceTime || maxServiceTime < 1 || minServiceTime < 1 || maxServiceTime > 99999 || minServiceTime > 99999)
                return false;
            if(maxArrivalTime <= minArrivalTime || maxArrivalTime < 1 || minArrivalTime < 1 || maxArrivalTime > 99999 || minArrivalTime > 99999)
                return false;
            if(maxArrivalTime > maxTime) return false;

            return true;


        } catch (Exception e){
            return false;
        }

    }



    @FXML
    protected synchronized void runSimulation(MouseEvent event) {

        if(!validInput()){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error Input");
            alert.setHeaderText(null);
            alert.setContentText("Ooops, invalid input!");

            alert.showAndWait();
            return;
        }

        currentTime = 0;
        timeOutput.setText("0");
        waitingOutput.setText("0");
        initOutputs();
        SimulationController runSim = new SimulationController(
                Integer.parseInt(maxTimeInput.getText()),
                Integer.parseInt(noQueuesInput.getText()),
                Integer.parseInt(noClientsInput.getText()),
                Integer.parseInt(maxServiceTimeInput.getText()),
                Integer.parseInt(minServiceTimeInput.getText()),
                Integer.parseInt(maxArrivalTimeInput.getText()),
                Integer.parseInt(minArrivalTimeInput.getText())
        );

        //init ui
        Platform.runLater(()->{
            SimulationFrameController.getFinalServers().forEach(o -> {
                QueuesHBox.getChildren().add(new QueueDisplay(o));
            });
        });

        SimulationFrameController.changed.setValue(false);
        SimulationFrameController.done.setValue(false);
        disableAllInputs();
        runSim.start();

    }

    private void initOutputs() {
        peakHourOutput.setText("");
        avgServiceTimeOutput.setText("");
        avgWaitTimeOutput.setText("");
    }

    @FXML
    public synchronized void initialize() {
        timeOutput.setText("0");
        waitingOutput.setText("0");
        currentTime = 0;

        SimulationFrameController.changed.addListener((o, oldVal, newVal) -> {
            if (newVal && !oldVal)
                updateUIServers();
        });
        SimulationFrameController.done.addListener((o, oldVal, newVal) -> {
            if (newVal && !oldVal)
                cleanSimulation();
        });
    }

    private synchronized void cleanSimulation() {

        SimulationFrameController.pauseExecution.setValue(true);

        timeOutput.setText("0");
        waitingOutput.setText("0");
        currentTime = 0;

        Platform.runLater(()->{

            QueuesHBox.getChildren().clear();
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Simulation Dialog");
            alert.setHeaderText(null);
            alert.setContentText("The simulation is complete!\nLog file available!");

            alert.showAndWait();
        });

        activateAllInputs();
        printOutputs();
        SimulationFrameController.done.setValue(false);
        SimulationFrameController.pauseExecution.setValue(false);


    }

    private void printOutputs() {
        peakHourOutput.setText(String.valueOf(SimulationFrameController.getPeakHour()));
        avgServiceTimeOutput.setText(String.valueOf(SimulationFrameController.getAverageServiceTime()));
        avgWaitTimeOutput.setText(String.valueOf(SimulationFrameController.getAverageWaitingTime()));
    }

    private synchronized void updateUIServers() {

        if (!SimulationFrameController.done.get()) {

            SimulationFrameController.pauseExecution.setValue(true);

            Platform.runLater(() ->{
                QueuesHBox.getChildren().forEach(q ->{
                    ((QueueDisplay)q).updateServer();
                });

            });

            waitingLine = SimulationFrameController.getWaitingListSize();
            timeOutput.setText(currentTime.toString());
            waitingOutput.setText(String.valueOf(waitingLine));
            currentTime++;


            try {
                SimulationFrameController.changed.setValue(false);
                SimulationFrameController.pauseExecution.setValue(false);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }
}