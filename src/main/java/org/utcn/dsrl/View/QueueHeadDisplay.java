package org.utcn.dsrl.View;

import javafx.scene.control.Button;
import org.utcn.dsrl.Model.Task;

public class QueueHeadDisplay extends Button {

    public QueueHeadDisplay(Integer tasksSize, Integer waitingPeriodInteger) {
        super();
        this.setText("tasks " + tasksSize.toString() + "\nwait time " + waitingPeriodInteger.toString());
        this.getStylesheets().add(UserInterfaceController.class.getResource("style.css").toExternalForm());
        this.getStyleClass().add("queue-head-display");
    }
}
