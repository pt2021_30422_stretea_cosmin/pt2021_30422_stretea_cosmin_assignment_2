package org.utcn.dsrl.View;

import javafx.application.Platform;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.scene.control.Button;
import javafx.scene.layout.VBox;
import org.utcn.dsrl.Model.Server;
import org.utcn.dsrl.Model.Task;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.LinkedBlockingQueue;

public class QueueDisplay extends VBox {

    private Server server;
    private LinkedBlockingQueue<Task> tasks;

    private VBox progress;
    private Button headQ;

    private IntegerProperty taskCount = new SimpleIntegerProperty(0);

    public Server getServer() {
        return server;
    }

    public void setServer(Server server) {
        this.server = server;
    }

    public QueueDisplay(Server s){
        super();

        this.server = s;
        this.tasks = s.getTasks();
        this.tasks.forEach(o -> {
            this.getChildren().add(new TaskDisplay(o));
        });

        //add button
//        this.headQ = new Button();
//        this.headQ.setPrefHeight(10);
//        this.headQ.setPrefWidth(16);
//        this.headQ.setStyle("-fx-background-color: red");
//        this.getChildren().add(this.headQ);
//        this.headQ.setWrapText(true);

//        this.progress = new VBox();
//        this.progress.setMaxHeight(9999999);
//        this.progress.setStyle("-fx-background-color: red");
//        this.getChildren().add(this.progress);
        this.getStylesheets().add(UserInterfaceController.class.getResource("style.css").toExternalForm());
        this.getStyleClass().add("queue-display");

        this.taskCount.addListener( (o, oldVal, newVal) ->{
            if(newVal.intValue() >= 0){
                updateServer();
            }
        });
    }

    public LinkedBlockingQueue<Task> getTasks() {
        return tasks;
    }

    public synchronized void updateServer() {
//        Platform.setImplicitExit(false);
//        Platform.runLater(()->{
////            this.getChildren().clear();
////            Button newQ = new Button(server.getTasksSize().toString());
////            newQ.setWrapText(true);
////            this.getChildren().add(newQ);
////            server.getTasks().forEach(t -> {
////                this.getChildren().add(new TaskDisplay(t));
////            });
//            this.headQ.setText(server.getTasksSize().toString());
//            this.progress.setPrefHeight(server.getTasksSize() * 5);
//        });
        Platform.runLater(() -> {
            this.getChildren().clear();
            QueueHeadDisplay head = new QueueHeadDisplay(server.getTasksSize(), server.getWaitingPeriodInteger());
            this.getChildren().add(head);
            server.getTasks().forEach(t -> {
                this.getChildren().add(new TaskDisplay(t));
            });

//            taskCount.setValue(server.getTasksSize());
//            this.headQ.setText(server.getTasksSize().toString());
//            this.progress.setPrefHeight(server.getTasksSize() * 5);
        });

    }
}
